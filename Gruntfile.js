module.exports = function(grunt) {

    grunt.initConfig({
        // pkg: require('./package.json'),

        dirConfig: {
            dev: 'dev',
            prod: 'htdocs',
            styleguide: 'styleguide/dist'
        }
    });

    grunt.loadTasks('grunt-tasks');

    grunt.registerTask('default', ['watch']);
    grunt.registerTask('build', ['htmlhint', 'jshint', 'clean', 'requirejs', 'copy:html', 'imagemin', 'compass', 'autoprefixer', 'ftpscript:all']);
};
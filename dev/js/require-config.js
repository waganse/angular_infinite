require.config({

  baseUrl: 'js',

  paths: {
    'jquery': 'lib/jquery-1.9.1',
    'trunk': 'lib/jquery-trunk8',
    'angular': 'lib/angular',
    'infinite-scroll': 'lib/ng-infinite-scroll'
  },

    shim: {
        'trunk': {
            'deps': ['jquery']
        },
        'angular': {
            'exports': 'angular'
        }
    }

});
require([
    'angular',
    'app',
    'controller'
], function(angular) {

    return angular.bootstrap(document, ['pickupApp']);

});

define([
    'jquery',
    'app',
    'trunk'
], function($, app) {
    app.directive('trunk', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                setTimeout(function() {
                    $(element).find('.js-trunk').trunk8(scope.$eval(attrs.trunk));
                }, 0);
            }
        };
    });

    app.controller('pickupController', function($scope, $rootScope) {
        $rootScope.f_page = 0;
        $rootScope.m_page = 0;
        $rootScope.f_genreId = '100371';
        $rootScope.m_genreId = '551177';
        $rootScope.f_interval = 3;
        $rootScope.m_interval = 4;
        $rootScope.url = 'https://app.rakuten.co.jp/services/api/IchibaItem/Search/20130801?format=json&callback=JSON_CALLBACK&tagId=5000001&applicationId=rba';
    });

    app.controller('pickupController1', function($scope, f_Loader) {
        $scope.loader = new f_Loader();
    });

    app.controller('pickupController2', function($scope, m_Loader) {
        $scope.loader = new m_Loader();
    });

    app.controller('pickupController3', function($scope, f_Loader) {
        $scope.loader = new f_Loader();
    });

    app.controller('pickupController4', function($scope, m_Loader) {
        $scope.loader = new m_Loader();
    });

    app.service('f_Loader', function($http, $rootScope) {
        var Loader = function() {
            this.items = [];
            this.busy = false;
            this.itemNum = 0;
        };

        Loader.prototype.loadNext = function() {
            var self = this,
                url = $rootScope.url + '&genreId=' + $rootScope.f_genreId + '&page=';

            $rootScope.f_page++;

            url += $rootScope.f_page;

            if (self.busy) { return; }

            self.busy = true;

            $http.jsonp(url).success(function(data) {
                var items = data.Items;

                $.each(items, function(i, itemObj) {
                    var item = itemObj.Item;

                    self.itemNum++;

                    if (self.itemNum % $rootScope.f_interval === 1) {
                        item.isBig = true;
                        item.className = 'big';
                    } else {
                        item.className = '';
                    }
                    self.items.push(item);
                });

                self.busy = false;
            });
        };

        return Loader;
    });

    app.service('m_Loader', function($http, $rootScope) {
        var Loader = function() {
            this.items = [];
            this.busy = false;
            this.itemNum = 0;
        };

        Loader.prototype.loadNext = function() {
            var self = this,
                url = $rootScope.url + '&genreId=' + $rootScope.m_genreId + '&page=';

            $rootScope.m_page++;

            url += $rootScope.m_page;

            if (self.busy) { return; }

            self.busy = true;

            $http.jsonp(url).success(function(data) {
                var items = data.Items;

                $.each(items, function(i, itemObj) {
                    var item = itemObj.Item;

                    self.itemNum++;

                    if (self.itemNum % $rootScope.m_interval === 0) {
                        item.isBig = true;
                        item.className = 'big';
                    } else {
                        item.className = '';
                    }
                    self.items.push(item);
                });

                self.busy = false;
            });
        };

        return Loader;
    });
});

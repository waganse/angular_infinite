define([
    'angular',
    'infinite-scroll',
], function(angular) {

    var app;

    app = angular.module('pickupApp', ['infinite-scroll']).config(function($sceProvider) {
      // Completely disable SCE to support IE7.
      $sceProvider.enabled(false);
    });

    return app;

});

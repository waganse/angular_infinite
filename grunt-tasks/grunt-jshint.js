module.exports = function(grunt) {
    grunt.config('jshint', {
        options: {
            jshintrc: '.jshintrc',
            jshintignore: '.jshintignore'
        },
        src: ['<%= dirConfig.dev %>/js/*.js']
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
};
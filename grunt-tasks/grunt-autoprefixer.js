module.exports = function(grunt) {
    grunt.config('autoprefixer', {
        options: {
            browsers: ['last 2 versions', 'ie 7', 'ie 8']
        },
        file: {
          expand: true,
          flatten: true,
          src: '<%= dirConfig.dev %>/css/*.css',
          dest: '<%= dirConfig.prod %>/css'
        }
    });

    grunt.loadNpmTasks('grunt-autoprefixer');
};
module.exports = function(grunt) {
    grunt.config('copy', {
        html: {
            cwd: '<%= dirConfig.dev %>',
            src: ['*.html'],
            dest: '<%= dirConfig.prod %>/',
            filter: 'isFile',
            expand: true
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
};
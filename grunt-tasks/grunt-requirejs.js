module.exports = function(grunt) {
    grunt.config('requirejs', {
        main: {
            options: {
                almond: true,
                mainConfigFile: '<%= dirConfig.dev %>/js/require-config.js',
                baseUrl: '<%= dirConfig.dev %>/js',
                name : 'main',
                out: '<%= dirConfig.prod %>/js/loading.min.js',
                // optimize: 'uglify',
                optimize: 'none',
                preserveLicenseComments: true
            }
        }
    });

    grunt.loadNpmTasks('grunt-requirejs');
};
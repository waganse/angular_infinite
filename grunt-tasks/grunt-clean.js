module.exports = function(grunt) {
    grunt.config('clean', {
        html: {
            src: ['<%= dirConfig.prod %>/html/*']
        },
        css: {
            src: ['<%= dirConfig.dev %>/css/*', '<%= dirConfig.prod %>/css/*']
        },
        js: {
            src: ['<%= dirConfig.prod %>/js/*']
        },
        img: {
            src: ['<%= dirConfig.prod %>/img/*']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
};
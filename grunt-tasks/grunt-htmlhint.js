module.exports = function(grunt) {
    grunt.config('htmlhint', {
        options: {
            htmlhintrc: '.htmlhintrc'
        },

        html: {
            src: ['<%= dirConfig.dev %>/*.html']
        }
    });

    grunt.loadNpmTasks('grunt-htmlhint');
};
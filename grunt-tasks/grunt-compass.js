module.exports = function(grunt) {
    grunt.config('compass', {
        main: {
            options: {
                config: 'config.rb'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-compass');
};
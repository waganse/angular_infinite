module.exports = function(grunt) {
    grunt.config('watch', {
        options: {
            nospawn: true,
            livereload: true
        },

        html: {
            files: ['<%= dirConfig.dev %>/*.html'],
            tasks: ['htmlhint', 'clean:html', 'copy:html', 'ftpscript:html']
        },

        img: {
            files: ['<%= dirConfig.dev %>/img/*'],
            tasks: ['clean:img', 'imagemin', 'ftpscript:img']
        },

        compass: {
            files: ['<%= dirConfig.dev %>/sass/*.scss', '<%= dirConfig.dev %>/sass/**/*.scss'],
            tasks: ['clean:css', 'compass', 'autoprefixer', 'ftpscript:css']
            // tasks: ['clean:css', 'compass', 'autoprefixer', 'cssmin']
        },

        requirejs: {
            files: ['<%= dirConfig.dev %>/js/*.js', '<%= dirConfig.dev %>/js/lib/*.js'],
            tasks: ['jshint', 'clean:js', 'requirejs', 'ftpscript:js']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
};
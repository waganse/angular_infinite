module.exports = function(grunt) {
    grunt.config('uglify', {
        target: {
            files: {
                '<%= dirConfig.prod %>/js/loading.min.js': ['<%= dirConfig.dev %>/js/*.js', '<%= dirConfig.dev %>/js/lib/*.js']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
};
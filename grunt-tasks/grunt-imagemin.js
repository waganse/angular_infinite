module.exports = function(grunt) {
    grunt.config('imagemin', {
        dynamic: {
            options: {
                optimizationLevel: 3
            },
            files: [{
                expand: true,
                cwd: '<%= dirConfig.dev %>/',
                src: ['img/*.png', 'img/*.jpg', 'img/*.gif'],
                dest: '<%= dirConfig.prod %>'
            }]
        }
    });

    grunt.loadNpmTasks('grunt-contrib-imagemin');
};
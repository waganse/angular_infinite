module.exports = function(grunt) {
    var path = require('path'),
        lrSnippet = require('grunt-contrib-livereload/lib/utils').livereloadSnippet,
        folderMount = function(connect, dir) {
            return connect.static(path.resolve(dir));
        };

    grunt.config('connect', {
        server: {
            options: {
                port: '9999',
                hostname: '0.0.0.0',
                middleware: function(connect) {
                    return [
                        lrSnippet,
                        folderMount(connect, 'htdocs')
                    ];
                },
                keepalive: true,
                livereload: true,
                open: true
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-connect');
};
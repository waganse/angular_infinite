module.exports = function(grunt) {
    var ftpHost = 'cwd-web',
        ftpPath = '/rba/top/pc';

    grunt.config('ftpscript', {
        all: {
            options: {
                host: ftpHost
            },
            files: [
                {
                    expand: true,
                    cwd: '',
                    src: [
                        '<%= dirConfig.prod %>/**'
                    ],
                    dest: ftpPath
                }
            ]
        },
        js: {
            options: {
                host: ftpHost
            },
            files: [
                {
                    expand: true,
                    cwd: '',
                    src: [
                        '<%= dirConfig.prod %>/js/*.js'
                    ],
                    dest: ftpPath
                }
            ]
        },
        css: {
            options: {
                host: ftpHost
            },
            files: [
                {
                    expand: true,
                    cwd: '',
                    src: [
                        '<%= dirConfig.prod %>/css/*.css'
                    ],
                    dest: ftpPath
                }
            ]
        },
        img: {
            options: {
                host: ftpHost
            },
            files: [
                {
                    expand: true,
                    cwd: '',
                    src: [
                        '<%= dirConfig.prod %>/img/*'
                    ],
                    dest: ftpPath
                }
            ]
        },
        html: {
            options: {
                host: ftpHost
            },
            files: [
                {
                    expand: true,
                    cwd: '',
                    src: [
                        '<%= dirConfig.prod %>/*.html'
                    ],
                    dest: ftpPath
                }
            ]
        }
    });

    grunt.loadNpmTasks('grunt-ftpscript');
};